There are a lot of key features that a distributed sy

Larry wrote an article called What Decentralization Requires. After some
conversations, thinking through this, and answering questions about this idea,
I've come tothe conclusion that The first principle he specifies there, the
data-ownership rule, is really the most important. It immediately forces a set
of design choices.

Core technical principle:

> If you like your data, you can keep your data. But as soon as you put your
> data on someone else's computer, it's no longer your data.

Now let us walk through some examples to illustrate how this forces design
choices.

#### Usage case 1: sharing a graduation video

Your kid is graduating from high school. You don't want to make the video
public, but you do want to share it with your family.

At the moment, the only way to do that is to put the video on a social
media site like YouTube or Facebook and then share the link. That means
Facebook or YouTube owns the data.

From the technological standpoint, there is currrently no way to share data
without surrendering ownership of the data. We can't completely fix that.
However, we can certainly improve the situation a lot.

What we can do is give you a way to streamtorrent the video, on-demand to your
friends, with access control, and without ever having to upload it to some
central system like YouTube or Facebook. There's a distributed caching system
so that your home internet connection doesn't get overloaded.

We can use a pretty straightforward cryptography scheme to implement access
control. So even if someone intercepts the stream, if they don't have the
correct key, they can't read it.

We can't stop people on the receiving end from recording the stream, or compel
them to delete the stream. Nor should we attempt to, as the only way to do so
is to implement decentralized DRM.

There's a ton of issues in making a system like this user-friendly, such as
letting your audience know that the video exists. But it's doable.

#### Usage case 2: Joe Rogan Podcast Stream

Joe Rogan wants to stream his podcast, and he doesn't want to worry about
censorship.

In this case, what OPSS can offer is streamtorrenting. Rogan streams from his
lair. Viewers get data both from Rogan's lair and from each other.

#### Usage case 3: The Decentralizers Forum

This case also covers something like Twitter or Facebook.

These types of situations are more challenging, because it's not as clear who
should own what data. There's tradeoffs to be made. One goal of OPSS is to make
these tradeoffs plainly visible, and allow an application developer to cleanly
choose which side he wants to take.

The tradeoff here is performance versus privacy. Here's the two extreme ends of
the tradeoff

- **Performance side** (make things fast).
  You would basically copy what Wordpress or BBS does: have all the data on one
  central node. Users don't own the data they input into the system. Maybe you
  use OPSS to make the website load faster and to solve the Slashdot effect.

- **Privacy side**.
  All posts by `$user` are served by `$user`'s home machine. So if his power is
  out, or he turns his machine off, his posts aren't visible. The central
  server stores the comment tree data structure, which merely points to
  addresses where people's comments come from, but doesn't store the comments.
  Users can delete/edit posts or go dark simply by turning their computer off
  (plus or minus cache delay).

## What these all have in common (the problem that OPSS solves)

At the core of all of these systems, there's some very basic problems:

1. Where do bits live?
2. How do you get bits from point A to point B?
3. How do you handle the Slashdot effect (servers melt from going viral)?
4. How do you implement access control?
5. How do you make the system user-friendly?
6. How do you implement social network features like aggregation,
   content recommendation, and "you might like this person/channel/account"?

OPSS solves issues 1-4. No existing open-source distributed system can make
that claim. Issues 5 and 6 need to be solved on an application-by-application
basis.

The best OPSS can do on #5 is to make OPSS as invisible to end users as
possible, and make itself as easy to use by application developers as possible.

Issues 1-4 have the properties that

1. They are difficult to do correctly
2. Most developers find them boring
3. Making a mistake is very costly
4. The requirements are exactly the same in every application
5. Everyone using the same solution is concave: the whole is greater than the
   sum of its parts.

Property (5) is true of OPSS for the same reason that it's true of BitTorrent:
the more people who are torrenting something, the faster it is to torrent it.
The more people who are using OPSS, the faster it is. It would be quite the
waste for a each application to implement its own distributed data store,
because it's faster if they're all using the same system.

There's a lot of ways to skin a cat. We're selling knives.
